package estruturas;

public interface ILista<T> extends IEstrutura<T> {
	public boolean search(T objeto);
}
