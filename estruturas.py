from abc import abstractclassmethod


class Elemento:
    def __init__(self, objeto):
        self.objeto = objeto
        self.proximo = None
        self.anterior = None


class Estrutura:

    @abstractclassmethod
    def pop(self):
        raise NotImplementedError

    @abstractclassmethod
    def push(self, objeto):
        raise NotImplementedError


class Pilha(Estrutura):
    def __init__(self):
        self.topo = None

    def push(self, objeto):
        novo = Elemento(objeto)
        novo.proximo = self.topo
        self.topo = novo

    def pop(self):
        if self.topo:
            velho = self.topo
            self.topo = self.topo.proximo
            return velho.objeto
        return None


class Fila(Estrutura):

    def __init__(self):
        self.primeiro = None
        self.ultimo = None

    def push(self, objeto):
        novo = Elemento(objeto)
        if self.ultimo:
            self.ultimo.proximo = novo
            self.ultimo = novo
        else:
            self.primeiro = novo
            self.ultimo = novo

    def pop(self):
        if self.primeiro:
            objeto = self.primeiro.objeto
            self.primeiro = self.primeiro.proximo
            return objeto
        else:
            return None


class Hash(Estrutura):

    def __init__(self, carga, tamanho):
        self.carga, self.tamanho = carga, tamanho
        self.__array = [None] * (tamanho // carga)

    def push(self, objeto):
        raise NotImplementedError  # TODO implementar

    def pop(self):
        raise NotImplementedError  # TODO implementar

    def buscar(self, objeto):
        raise NotImplementedError  # TODO implementar

    def hashfy(self, id_obj):
        return id_obj % self.tamanho


class Lista(Estrutura):

    def __init__(self):
        raise NotImplementedError  # TODO implementar

    def push(self, objeto):
        raise NotImplementedError  # TODO implementar

    def pop(self):
        raise NotImplementedError  # TODO implementar


class ListaDupla(Estrutura):

    def __init__(self):
        raise NotImplementedError  # TODO implementar

    def push(self, objeto):
        raise NotImplementedError  # TODO implementar

    def pop(self):
        raise NotImplementedError  # TODO implementar
