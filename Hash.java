package estruturas;

public class Hash<T extends IGetID> {
	
	final int FATOR_CARGA, TAMANHO_ESPERADO;
	Lista<T>[] array;
	
	@SuppressWarnings("unchecked")
	Hash(int fator, int tamanhoEsperado){
		FATOR_CARGA = fator;
		TAMANHO_ESPERADO = tamanhoEsperado;
		array = new Lista[TAMANHO_ESPERADO / FATOR_CARGA];
	}
	
	T buscar(int id, T objeto){
		if(array[hashify(objeto.getID())] == null){
			return null;
		}
		Lista<T> lista = array[hashify(id)];
		lista.buscar(objeto);
		return lista.acessar();
	}
	
	void inserir(int id, T objeto){
		if(array[hashify(id)] == null){
			array[hashify(id)] = new Lista<T>();
		}
		Lista<T> lista = array[hashify(id)];
		lista.inserirUltimo(objeto);
	}
	
	void remover(int id, T objeto){
		if(array[hashify(id)] != null){
			Lista<T> lista = array[hashify(id)];
			lista.buscar(objeto);
			lista.excluir();
		}
	}
	
	int hashify(int id){
		return id % TAMANHO_ESPERADO;
	}
}
