package estruturas;

public class ListaCadeiaDupla<T> {
	
	Elemento<T> primeiro;
	Elemento<T> cursor;

	ListaCadeiaDupla() {
		primeiro = null;
		cursor = null;
	}

	T acessa() {
		if (cursor == null) {
			return null;
		}
		return cursor.getObjeto();
	}

	void busca(T objeto) {
		if (cursor == null) {
			return;
		}
		insereAntes(objeto);
		Elemento<T> fake = cursor.getAnterior();
		Elemento<T> buscador = cursor;
		while (buscador.getProximo().getObjeto() != fake.getObjeto()) {
			buscador = buscador.getProximo();
		}
		if (buscador == fake) {
			remove();
		} else {
			setCursor(buscador);
		}
	}

	void setPrimeiro(Elemento<T> primeiro) {
		this.primeiro = primeiro;
	}

	Elemento<T> getPrimeiro() {
		return primeiro;
	}

	void setCursor(Elemento<T> cursor) {
		this.cursor = cursor;
	}

	Elemento<T> getCursor() {
		return cursor;
	}

	void insereAntes(T objeto) {
		if (cursor == null) {
			inserePrimeiro(objeto);
		} else {
			Elemento<T> novo = new Elemento<T>(objeto);
			novo.setProximo(cursor);
			novo.setAnterior(cursor.getAnterior());
			cursor.setAnterior(novo);
			cursor.getAnterior().setProximo(novo);
		}
	}

	void insereDepois(T objeto) {
		if (cursor == null) {
			inserePrimeiro(objeto);
		} else {
			Elemento<T> novo = new Elemento<T>(objeto);
			novo.setProximo(cursor.getProximo());
			novo.setAnterior(cursor);
			cursor.setProximo(novo);
			cursor.getProximo().setAnterior(novo);
		}
	}

	void inserePrimeiro(T objeto) {
		Elemento<T> novo = new Elemento<T>(objeto);
		setPrimeiro(novo);
		setCursor(novo);
	}

	void remove() {
		if (cursor == null) {
			return;
		} else {
			cursor.getAnterior().setProximo(cursor.getProximo());
			cursor.getProximo().setAnterior(cursor.getAnterior());
			setCursor(cursor.getProximo());
		}
	}

	void avance(int n) {
		if (cursor == null) {
			return;
		}
		while (n > 0) {
			setCursor(cursor.getProximo());
		}
	}

	void retroceda(int n) {
		if (cursor == null) {
			return;
		}
		while (n > 0) {
			setCursor(cursor.getAnterior());
		}
	}

	void vaParaPrimeiro() {
		if (cursor == null) {
			return;
		}
		setCursor(primeiro);
	}

	void vaParaUltimo() {
		if (cursor == null) {
			return;
		}
		setCursor(primeiro.getAnterior());
	}
}
