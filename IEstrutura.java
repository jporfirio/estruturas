package estruturas;

public interface IEstrutura<T> {
	public void push(T objeto);
	public T pop();
}
