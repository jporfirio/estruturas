package estruturas;

class Pilha<T> {
	
	Elemento<T> topo;
	
	Pilha() {
		topo = null;
	}
	
	void Push(T objeto){
		Elemento<T> novo = new Elemento<T>(objeto);
		novo.setProximo(topo);
		topo = novo;
	}
	
	T Pop(){
		if(topo == null) return null;
		Elemento<T> velho = topo;
		topo = topo.getProximo();
		return velho.getObjeto();
	}
}
