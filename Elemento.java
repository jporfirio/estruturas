package estruturas;

public class Elemento<T> {
	
	T objeto;
	Elemento<T> proximo;
	Elemento<T> anterior;

	Elemento(T objeto) {
		setObjeto(objeto);
		setProximo(this);
		setAnterior(this);
	}

	void setObjeto(T objeto) {
		this.objeto = objeto;
	}

	void setProximo(Elemento<T> proximo) {
		this.proximo = proximo;
	}

	void setAnterior(Elemento<T> anterior) {
		this.anterior = anterior;
	}

	T getObjeto() {
		return objeto;
	}

	Elemento<T> getProximo() {
		return proximo;
	}

	Elemento<T> getAnterior() {
		return anterior;
	}
}
