package estruturas;

public class Fila<T> {

	T objeto;
	Elemento<T> primeiro, ultimo;
	
	Fila(){
		
	}
	
	void push(T objeto){
		Elemento<T> novo = new Elemento<T>(objeto);
		if (ultimo == null){
			primeiro = novo;
			ultimo = novo;
		}else{
			ultimo.setProximo(novo);
			ultimo = novo;
		}
	}
	
	T pop(){
		if(primeiro == null){
			return null;
		}else{
			T obj = primeiro.getObjeto();
			primeiro = primeiro.getProximo();
			return obj;
		}
	}
}
