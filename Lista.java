package estruturas;

public class Lista<T> {
	
	Elemento<T> primeiro, ultimo, cursor;
	
	Lista(){
		primeiro = null;
		ultimo = null;
		cursor = null;
	}
	
	T acessar(){
		return cursor.getObjeto();
	}
	
	void inserirDepois(T obj){
		Elemento<T> novo = new Elemento<T>(obj);
		novo.setProximo(cursor.getProximo());
		cursor.setProximo(novo);
	}
	
	void inserirAntes(T obj){
		buscarAnterior(cursor);
		inserirDepois(obj);
	}
	
	void buscarAnterior(Elemento<T> elemento){
		Elemento<T> buscador = primeiro;
		while(buscador != null){
			if(buscador.getProximo().equals(elemento)){
				cursor = buscador;
				break;
			}
			buscador = buscador.getProximo();
		}
	}
	
	void buscar(T objeto){
		Elemento<T> buscador = primeiro;
		while(buscador != null){
			if(buscador.getObjeto() == objeto){
				cursor = buscador;
				break;
			}
		}
	}

	void buscar(Elemento<T> elemento){
		Elemento<T> buscador = primeiro;
		while(buscador != null){
			if(buscador.equals(elemento)){
				cursor = buscador;
				break;
			}
			buscador = buscador.getProximo();
		}
	}
	
	void vaParaPrimeiro(){
		setCursor(primeiro);
	}
	
	void vaParaUltimo(){
		setCursor(ultimo);
	}
	
	void avance(int n){
		while(n > 0){
			cursor = cursor.getProximo();
			n--;
		}
	}
	
	void retroceda(int n){
		while(n > 0){
			buscarAnterior(cursor);
			n--;
		}
	}
	
	void excluir(){
		cursor.setProximo(cursor.getProximo().getProximo());
	}
	
	void excluir(Elemento<T> elemento){
		buscarAnterior(elemento);
		cursor.setProximo(cursor.getProximo().getProximo());
	}
	
	void inserirPrimeiro(T obj){
		Elemento<T> novo = new Elemento<T>(obj);
		novo.setProximo(primeiro);
		primeiro = novo;
	}
	
	void inserirUltimo(T obj){
		Elemento<T> novo = new Elemento<T>(obj);
		ultimo.setProximo(novo);
		ultimo = novo;
	}
	
	void setCursor(Elemento<T> elemento){
		cursor = elemento;
	}
	
	void setPrimeiro(Elemento<T> elemento){
		primeiro = elemento;
	}
	
	void setUltimo(Elemento<T> elemento){
		ultimo = elemento;
	}
}
